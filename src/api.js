const express = require('express');

const serverless = require('serverless-http');

const app = express();

const router = express.Router();

router.get('/', (req, res) => {
    var d = new Date();
    var heu = d.getHours();
    var min = d.getMinutes();
    var sec = d.getSeconds();
    var mil = d.getMilliseconds();
    var timeis = heu+":"+min+":"+sec+":"+mil;
    res.json({
        'time': timeis
    });
});

app.use('/.netlify/functions/api', router);

module.exports.handler = serverless(app);