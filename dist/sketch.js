
let result;

function preload() {
  result = loadJSON("https://apiclock.netlify.com/.netlify/functions/api", displayTimes);
}

function setup() {
  noCanvas();
}

function displayTimes() {
    createP("server: "+result.time);
    createP("client: "+hour()+":"+minute()+":"+second()+":"+millis());
}